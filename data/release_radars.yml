- name: GitLab 11.2 - Get started and iterate faster
  dates:
    - date: September 5
      region: North America
      time: 9am PDT / 4pm UTC
    - date: September 12
      region: EMEA
      time: 2pm CEST / 12pm UTC

  image: /images/webcast/release-radar.svg
  tagline: "Accelerate software delivery and see changes in real time with our Web IDE"
  description:
    - "Developing web applications is faster and easier when developers don’t have to set up a new project from scratch and can make changes in real time. In our latest release, the live preview in GitLab’s Web IDE allows users to iterate faster by seeing the outcome before committing the changes."
    - "To speed up the installation process, the Cloud Native Helm Chart is now generally available to help you get started with GitLab on Kubernetes. And manifest files help you move forward faster by allow you to import larger project structures with multiple repositories all together, for instance enabling the import of repositories based on the <a href='https://source.android.com/' target='blank'>Android Open Source Project (AOSP)</a>. Custom project templates allow users to get started faster by eliminating tedious set-up tasks."
    - "Join us for a live broadcast to learn more about GitLab’s latest enhancements including how to preview your live JavaScript web application in the Web IDE and see your changes in real time right next to the code."
  form: 1419
  what_we_cover:
    - "Live Preview in GitLab’s Web IDE"
    - "Custom Project Templates"
    - "Import Android projects"
  youtube_url: ""

- name: GitLab 11.1 - Security
  dates:
    - date: August 8
      region: North America
      time: 9am PDT / 4pm UTC
  image: /images/webcast/release-radar.svg
  tagline: "New dashboard brings increased visibility to your security team"
  description:
    - "Security is a critical component of the software development pipeline, and GitLab’s single application makes it easier for security professionals and developers to collaborate and resolve issues early. Our latest release offers improved visibility by way of a new Security Dashboard."
    - "The interactive dashboard details the latest security status of each project’s default branch. This new feature will make it even easier for security teams to identify problems that require action and dismiss false positives. Users can also create issues to solve existing vulnerabilities right from the dashboard."
    - "Join us for a live broadcast on August 8 to learn more about GitLab’s security features including the newly released dashboard."
  form: 1419
  what_we_cover:
    - "Our new security dashboard"
    - "A rundown of GitLab’s security features"
    - "A demo and walkthrough of the pipeline interface"
  youtube_url: "https://www.youtube.com/embed/P0a6pdCeg1A"

- name: GitLab 11.0 - Auto DevOps
  dates:
    - date: June 27
      region: North America
      time: 9am PDT / 4pm UTC
  image: /images/webcast/release-radar.svg
  tagline: "Accelerate delivery by 200% in two steps"
  description:
    - "How many steps does it take to go from code to production? Automated pipelines are supposed to make software delivery faster and more efficient, but often require many integrations that need to be managed and maintained—making the process not so automatic. However, with Auto DevOps, you can go from code to production in just two steps."
    - "Your pipeline is built into the same application as your repository, no integration necessary. Just write and commit your code, and Auto DevOps will do the rest: detect the language of your code and automatically build, test, measure code quality, scan for security issues, package, monitor, and deploy the application. Auto DevOps removes the barriers to shipping secure, bug-free code, fast."
    - "Join us for a live broadcast on June 27 to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users."
  form: 1419
  what_we_cover:
    - "The competitive edge imperative: speed, feedback, and responsiveness."
    - "What is Auto Devops and how does it work?"
    - "2-step demo and pipeline interface walk through."
  youtube_url: "https://www.youtube.com/embed/6LZQCCVGVDg"
