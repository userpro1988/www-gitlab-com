---
layout: markdown_page
title: "Integrating Security into DevSecOps"
---
## Application Security is hard - when it is a separate process

Do you find yourself picking one of these paths?
- Sidestep vulnerabilities and hope they don’t get exploited before they can be fixed?
- Invest in purchase, tuning and ongoing maintenance of a Web Application Firewall (WAF) to protect the vulnerabilities in production?
- Stop a push to production when a vulnerability is found, and hope the CEO doesn't fire you?
- Spend a fortune to test every iteration of code in every app? 

Balancing business velocity with security need not be a battle. 
- With GitLab security testing is built into the merge process.
- Every merge request is scanned for vulnerabilities in your code and that of your dependencies and licensing issues are identified.
- One source of truth for both the developer and the security pro.

[See how integration is the key to successful DevSecOps](https://about.gitlab.com/2018/09/11/what-south-africa-taught-me-about-cybersecurity/)

