---
layout: markdown_page
title: "2018 Q4 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: TBD IACV Goal

* VPE
  * Support:
    * EMEA:
    * APAC:
	* AMER East:
	* AMER West:

### CEO: TBD Product Goal

* VPE:
  * Frontend:
  * Dev Backend:
    * Plan: Preserve 100% of [error budget]: 100% ()
    * Plan: Complete phase 1 of preparedness for [Elasticsearch on GitLab.com]
  * Ops Backend:
  * Infrastructure:
  * Quality:
  * UX:
  * Security:

### CEO: TBD Team Goal

* VPE
  * Frontend:
  * Dev Backend:
    * Plan: Source 25 candidates (at least 5 by direct manager contact) by
      October 15 and hire 2 developers: X sourced (X%), X hired (X%)
  * Infrastructure:
  * Ops Backend:
  * Quality:
  * Security:
  * Support:
  * UX:

[error-budget]: /handbook/engineering/#error-budgets
[Elasticsearch on GitLab.com]: https://gitlab.com/groups/gitlab-org/-/epics/153
